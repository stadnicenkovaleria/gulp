import dartSass from "sass";
import gulpSass from "gulp-sass";
import groupCssMediaQueries from 'gulp-group-css-media-queries';
import webpcss from 'webpcss';
import autoprefixer from 'autoprefixer';
import cleanCss from 'clean-css';

const sass = gulpSass(dartSass);

export const scss = () => {
  return app.gulp.src(app.path.src.scss, { sourcemaps: app.isDev })
    .pipe(app.plugins.plumber(
        app.plugins.notify.onError({
          title: "JS",
          message: "Error: <%= error .message %>",
        })))
        .pipe(app.plugins.replace(/@img\//g, '../img/'))
        .pipe(sass({
          outputStyle: 'expanded'
        }))
        .pipe (
          app.plugins.if(
            app.isBuild,
            groupCssMediaQueries()
          )
        )
        // hhhh
        .pipe (
          app.plugins.if(
            app.isBuild,
            webpcss.default
          )
        )
        .pipe (
          app.plugins.if(
            app.isBuild,
            autoprefixer({
              grid: true,
              overrideBrowsersList: ["last 3 version"],
              cascade: true
            })
          )
        )

        .pipe(app.gulp.dest(app.path.build.css))
        .pipe(cleanCss())
        .pipe(rename({
          extname: "min.css"
        }))
        .pipe(app.gulp.dest(app.path.build.css))
        .pipe(app.plugins.browsersync.stream());
  }